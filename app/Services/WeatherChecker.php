<?php

namespace App\Services;

use App\Services\Weather\Mapper;
use App\Services\Weather\WeatherParserInterface;

class WeatherChecker
{

    /**
     * @var \App\Services\Weather\WeatherParserInterface $service
     */
    protected $service;

    /**
     * @var \App\Services\Weather\Mapper $mapper
     */
    protected $mapper;

    /**
     * @param \App\Services\Weather\WeatherParserInterface $service
     * @param \App\Services\Weather\Mapper $mapper
     */
    public function __construct(
        WeatherParserInterface $service,
        Mapper $mapper
    ) {
        $this->service = $service;
        $this->mapper = $mapper;
    }

    /**
     * @param string $city
     *
     * @return array|null
     */
    public function getInfoByName(string $city)
    {
        $result = $this->service->getInfoByName($city);
        if(null !== $result){
            return $this->mapper->map($result);
        }

        return null;

    }

}