<?php

namespace App\Services;

use App\Models\City;
use App\Models\Country;
use Illuminate\Database\Eloquent\Collection;

class CityService
{

    /**
     * @var \App\Services\WeatherChecker $checker
     */
    protected $checker;

    /**
     * @param \App\Services\WeatherChecker $checker
     */
    public function __construct(WeatherChecker $checker)
    {
        $this->checker = $checker;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getCitiesFromDatabase(): Collection
    {
        $cityModel = new City();
        $cities = $cityModel->newQuery()
            ->select(['cities.*', 'countries.code'])
            ->join('countries','cities.country_id','=','countries.id')
            ->orderByDesc('created_at')
            ->limit(100)
            ->get();
        $this->hydrateWithWeatherData($cities);

        return $cities;
    }

    /**
     * @param string $city
     *
     * @return bool
     */
    public function addCity(string $city): bool
    {
        $data = $this->checker->getInfoByName($city);
        if($data){
            $countryModel = new Country();
            $country = $countryModel->newQuery()->select()->where(['code' => $data['countryCode']])->first();
            if(!$country){
                $countryModel->code = $data['countryCode'];
                $countryModel->name = $data['countryName'];
                $countryModel->save();
                $country = $countryModel;
            }
            $cityModel = new City();
            $city = $cityModel->newQuery()->select()->where(['name' => $data['cityName']])->first();
            if(!$city){
                $cityModel->latitude = $data['latitude'];
                $cityModel->longitude = $data['longitude'];
                $cityModel->name = $data['cityName'];
                $cityModel->country_id = $country->id;
                $cityModel->save();

                return true;
            }
        }

        return false;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection $cities
     *
     * @return void
     */
    protected function hydrateWithWeatherData(Collection $cities): void
    {
        foreach($cities as $city){
            /** @var \App\Models\City $city */
            if(empty($city->weather)){
                $city->weather = $this->checker->getInfoByName($city->name);
            }
        }
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection $cities
     *
     * @return \App\Models\City|null
     */
    public function getBestCity(Collection $cities)
    {
        $hottestCity = null;
        $hottestTemperature = null;
        foreach($cities as $city){
            /** @var \App\Models\City $city */
            if ($city->weather['temperature'] > $hottestTemperature){
                $hottestTemperature = $city->weather['temperature'];
                $hottestCity = $city;
            }
        }

        return $hottestCity;
    }

}