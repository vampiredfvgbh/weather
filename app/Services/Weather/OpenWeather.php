<?php

namespace App\Services\Weather;

use Exception;
use GuzzleHttp\Client;

class OpenWeather implements WeatherParserInterface
{

    const URL = 'https://api.openweathermap.org/data/2.5/weather?q=%s&APPID=%s';

    /**
     * @param string $name
     *
     * @return array|null
     */
    public function getInfoByName(string $name)
    {
        try {
            $url = sprintf(static::URL, urlencode($name), $this->getAppId());
            $client = new Client();
            $result = $client->get($url);
            if ($result->getStatusCode() === 200) {
                return $this->parse($result->getBody());
            }
        } catch (Exception $e) {

        }


        return null;
    }

    /**
     * @return string
     */
    protected function getAppId(): string
    {
        return config('weather.open_weather.app_id');
    }

    /**
     * @param string $response
     *
     * @return array
     */
    protected function parse(string $response): array
    {
        $response = json_decode($response, true);
        return [
            'latitude' => $response['coord']['lat'],
            'longitude' => $response['coord']['lon'],
            'temperature' => (float)$response['main']['temp'] - 273.15,
            'pressure' => $response['main']['pressure'],
            'wind' => $response['wind']['speed'],
            'cityName' => $response['name'],
            'countryCode' => $response['sys']['country'],
            'countryName' => $response['sys']['country'],
        ];
    }


}