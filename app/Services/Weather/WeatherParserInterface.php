<?php

namespace App\Services\Weather;

interface WeatherParserInterface
{

    /**
     * @param string $name
     *
     * @return array|null
     */
    public function getInfoByName(string $name);

}