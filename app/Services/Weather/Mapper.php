<?php

namespace App\Services\Weather;

class Mapper
{

    /**
     * @param array $data
     *
     * @return array
     */
    public function map(array $data): array
    {
        return [
            'temperature' => $data['temperature'],
            'pressure' => $data['pressure'],
            'wind' => $data['wind'],
            'cityName' => $data['cityName'],
            'countryName' => $data['countryName'],
            'countryCode' => $data['countryCode'],
            'latitude' => $data['latitude'],
            'longitude' => $data['longitude'],
        ];
    }

}

