<?php

namespace App;

use App\Models\City;
use App\Models\Country;
use App\Services\CityService;
use App\Services\Weather\Mapper;
use App\Services\Weather\OpenWeather;
use App\Services\Weather\WeatherParserInterface;
use App\Services\WeatherChecker;

class Factory
{

    /**
     * @return \App\Models\City
     */
    public function createCityModel(): City
    {
        return new City();
    }

    /**
     * @return \App\Services\WeatherChecker
     */
    public function createWeatherChecker(): WeatherChecker
    {
        return new WeatherChecker(
            $this->createWeatherParser(),
            $this->createWeatherMapper()
        );
    }

    /**
     * @return \App\Services\Weather\WeatherParserInterface
     */
    public function createWeatherParser(): WeatherParserInterface
    {
        return new OpenWeather();
    }

    /**
     * @return \App\Services\Weather\Mapper
     */
    public function createWeatherMapper(): Mapper
    {
        return new Mapper();
    }

    /**
     * @return \App\Models\Country
     */
    public function createCountryModel(): Country
    {
        return new Country();
    }

    /**
     * @return \App\Services\CityService
     */
    public function createCitiesService(): CityService
    {
        return new CityService(
            $this->createWeatherChecker()
        );
    }

}