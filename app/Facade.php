<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;

class Facade
{

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getCitiesFromDatabase(): Collection
    {
        return $this->getFactory()->createCitiesService()->getCitiesFromDatabase();
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection $cities
     *
     * @return \App\Models\City|null
     */
    public function getBestCity(Collection $cities)
    {
        return $this->getFactory()->createCitiesService()->getBestCity($cities);
    }

    /**
     * @param string $city
     *
     * @return bool
     */
    public function addCity(string $city): bool
    {
        return $this->getFactory()->createCitiesService()->addCity($city);
    }

    /**
     * @return \App\Factory
     */
    protected function getFactory(): Factory
    {
        return new Factory();
    }

}