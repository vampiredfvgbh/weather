<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 15 Sep 2018 17:29:22 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Country
 * 
 * @property int $id
 * @property string $name
 * @property string $code
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $cities
 *
 * @package App\Models
 */
class Country extends Eloquent
{
	protected $fillable = [
		'name',
		'code'
	];

	public function cities()
	{
		return $this->hasMany(\App\Models\City::class);
	}
}
