<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 15 Sep 2018 17:29:22 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class City
 * 
 * @property int $id
 * @property int $country_id
 * @property string $name
 * @property float $latitude
 * @property float $longitude
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Country $country
 *
 * @package App\Models
 */
class City extends Eloquent
{
	protected $casts = [
		'country_id' => 'int',
		'latitude' => 'float',
		'longitude' => 'float'
	];

	protected $fillable = [
		'country_id',
		'name',
		'latitude',
		'longitude'
	];

	public function country()
	{
		return $this->belongsTo(\App\Models\Country::class);
	}
}
