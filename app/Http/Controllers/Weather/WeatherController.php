<?php

namespace App\Http\Controllers\Weather;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class WeatherController extends Controller
{

    public $layout = 'layouts.default';

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index(Request $request)
    {
        if($request->isMethod('post')){
            $search = $request->input('search');
            if(!$search){
                Session::flash('alert-warning', 'Search string is empty');
            } else {
                $result = $this->getFacade()->addCity($search);
                if(!$result){
                    Session::flash('alert-warning', 'City either exists or not found');
                } else {
                    Session::flash('alert-info', 'City was successfully added');
                }
            }
        }
        $cities = $this->getFacade()->getCitiesFromDatabase();
        $bestCity = $this->getFacade()->getBestCity($cities);

        return $this->view('weather.cities', [
            'cities' => $cities,
            'bestCity' => $bestCity,
        ]);
    }

}
