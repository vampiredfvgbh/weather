<?php

namespace App\Http\Controllers;

use App\Facade;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $layout = 'layouts.default';

    /**
     * @return \App\Facade
     */
    protected function getFacade(): Facade
    {
        return new Facade();
    }

    /**
     * @param string $template
     * @param array $data
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    protected function view(string $template, array $data = [])
    {
        return view($template, [
            'layout' => $this->layout,
            'data' => $data,
        ]);
    }

}
