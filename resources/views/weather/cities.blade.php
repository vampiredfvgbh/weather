@extends($layout)

@section('content')
    <h1>Search for a city to add to weather list</h1>
    {{ Form::open(['url' => '/']) }}
    <div class="form-group">
        {{ Form::label('Input any geo information') }}
        {{ Form::text('search', null, ['class'=>'form-control', 'placeholder' => 'City...']) }}
    </div>
    <div class="form-group">
        {{ Form::submit('Search', null, ['class'=>'form-control']) }}
    </div>
    {{ Form::close() }}

    @if ($data['bestCity'])
        <h1>Hottest City</h1>
        <li class="list-group-item">
            <h3>{{ $data['bestCity']['name'] }}, {{ $data['bestCity']['code'] }}</h3>
            Temperature: {{ $data['bestCity']['weather']['temperature'] }}
            Pressure: {{ $data['bestCity']['weather']['pressure'] }}
            Wind: {{ $data['bestCity']['weather']['wind'] }}
        </li>
    @endIf

    <h1>Weather in different cities</h1>
    <ul class="list-group">
        @foreach ($data['cities'] as $city)
            <li class="list-group-item">
                <h3>{{ $city['name'] }}, {{ $city['code'] }}</h3>
                Temperature: {{ $city['weather']['temperature'] }}
                Pressure: {{ $city['weather']['pressure'] }}
                Wind: {{ $city['weather']['wind'] }}
            </li>
        @endforeach
    </ul>

@endsection