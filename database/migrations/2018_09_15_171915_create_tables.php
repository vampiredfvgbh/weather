<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTables extends Migration
{

    const CITIES_TABLE = 'cities';
    const CITIES_TABLE_FK_COUNTRIES = 'cities_country_id_fk';
    const COUNTRIES_TABLE = 'countries';


    /**
     * @return void
     */
    public function up(): void
    {
        if(!Schema::hasTable(static::COUNTRIES_TABLE)){
            Schema::create(static::COUNTRIES_TABLE, function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 50);
                $table->string('code', 2);
                $table->timestamps();
                $table->unique('name');
                $table->unique('code');
            });
        }

        if(!Schema::hasTable(static::CITIES_TABLE)) {
            Schema::create(static::CITIES_TABLE, function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('country_id');
                $table->string('name', 50);
                $table->float('latitude', 9, 6);
                $table->float('longitude', 9, 6);
                $table->timestamps();

                $table->index('country_id');
                $table->foreign('country_id', static::CITIES_TABLE_FK_COUNTRIES)
                    ->references('id')
                    ->on(static::COUNTRIES_TABLE)
                    ->onDelete('cascade')
                    ->onDelete('cascade');
                $table->unique(['latitude', 'longitude']);
                $table->unique('name');
            });
        }
    }

    /**
     * @return void
     */
    public function down(): void
    {
        if(Schema::hasTable(static::CITIES_TABLE)) {
            Schema::table(static::CITIES_TABLE, function (Blueprint $table) {
                $table->dropForeign(static::CITIES_TABLE_FK_COUNTRIES);
            });
        }
        Schema::dropIfExists(static::CITIES_TABLE);
        Schema::dropIfExists(static::COUNTRIES_TABLE);
    }

}
