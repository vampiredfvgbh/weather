<?php

$middleware = [];
if(Config::get('app.debug'))
{
    array_push($middleware, ['middleware' => 'clearcache']);
}

Route::group($middleware, function() {
    Route::match(['get', 'post'], '/', 'Weather\WeatherController@index');
});